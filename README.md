# Collection of Projects Project

This is a collection of projects I have done in my free time. 

* onlineGame is a game I wrote in the summer of 2017 as a way to learn python. Since then I have made some basic security fixes to the webcode. 

* machineLearningForPhysicists is a collection of Jupyter notebooks that come with the associated review paper. For the most part I have only made minor edits to the notebooks, tried to parse what exactly is being done, and moved on. Biggest output is the machineLearningSummary.md document. 

* Qiskit is a folder where I intend to collect future minor projects utilizing IBM's cloud quantum computers. 
