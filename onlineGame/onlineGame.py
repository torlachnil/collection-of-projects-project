import pygame
import sys
import random
from pygame.locals import *
import time
from time import sleep
from datetime import datetime
from threading import Event, Thread
import math
import random
import numpy as np
import scipy.ndimage
import scipy
import socket
import select
from multiprocessing import Process
import threading

# import allCorrectRunes
# Standing fundamental problems
# TODO: solve spawn issues introduced by security fix
# TODO: Implement the predetermined rune and accuracy feature. I expect:
#       1 commit to establish predef runes framework,
#       1 commit to implement path comparison
#       1 commit to integrate path comparison into spellcasting pipeline
# TODO: Remove use of deprecated Timer() function
# TODO: Find a less taxing background solution.
# TODO: FPSCLOCK.TICK was very slow at airport, current solution absolutely
#       awful at its job. # tfw pygame is shit, this will never be solved.
#       Game will just have to run at max 30 fps
#       Maybe even attempt separate Process for drawing? Just send it the entire
#       GAMEBOJECTS list periodically -> Concurrency!
#       It will probably introduce amazing bugs...
# TODO: write range check for reticle spells (that actually works)
# TODO: Make the game not reset until the whole group is dead
# TODO: No swirlies inside boss sand boss!
# TODO: Dash is even buggier than teleport, considering just giving players
#       high moveement speed instead...
# TODO: Is it possible to write some kind of decent testrig for this program?
#       A "multicomponent test" that spawns a bounch of units inte the same
#       place and checks for crashes for example...

##### setting up server settings
port = 6666
size = 1024
#server = None
serverIP = '213.103.135.54'
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = socket.gethostname()
server.connect((serverIP, port))

##### Setting up pygame shit and clieantID
pygame.init()# Create the constants (go ahead and experiment with different values)
WINDOWWIDTH = 1280
WINDOWHEIGHT = 720
FPS = 30

BLANK = None
global FPSCLOCK, DISPLAYSURF, BASICFONT, RESET_SURF, RESET_RECT, NEW_SURF, NEW_RECT, SOLVE_SURF, SOLVE_RECT, NOTUSEFULCLOCK, GLOBALTIME, bgx, bgy, font
looseend = ''
FPSCLOCK = pygame.time.Clock()
CLOCK = pygame.time.Clock()
GLOBALTIME = time.clock()

bpp = 16
flags = DOUBLEBUF
DISPLAYSURF = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT),flags)
pygame.display.set_caption('THE GAME')
DISPLAYSURF.set_alpha(None)

bgx=0
bgy=0
# print(pygame.font.get_fonts())

fontsToTry = "myriadprobolditopentype, monospace"
font = pygame.font.SysFont(fontsToTry, int(25*WINDOWHEIGHT/720))
textfont = pygame.font.SysFont(fontsToTry, int(16*WINDOWHEIGHT/720))

# set up the colors
BLACK = (0,   0,   0)
WHITE = (255, 255, 255)
RED = (255,   0,   0)
GREEN = (0, 255,   0)
BLUE = (0,   0, 255)
GREY = (30,  30,  30)

global clientID
clientID = int(str(time.clock())[-5:])

global GAMEOBJECTS
GAMEOBJECTS = []

def main():
    print(clientID)
    pygame.init()
    startGame()
    pygame.event.set_blocked(pygame.MOUSEMOTION)
    pygame.event.set_blocked(pygame.MOUSEBUTTONDOWN)
    pygame.event.set_blocked(pygame.MOUSEBUTTONUP)
    #t = threading.Thread(target=drawLoop)
    #t.daemon = True
    #t.start()
    #print('no killerino')
    serverListener = RepeatedTimer(0.05, 0, recieve) #Maybe this works without killing game FPS?
    #  threading without understadning threads hype!
    startTime = time.clock()
    lastTime = time.clock()-1
    while True:
        #if (time.clock() - startTime) > (1 / FPS):
        #startTime = startTime + 1/FPS
        #print('------------- Simulation FPS -----------------',1 / (time.clock() - lastTime))
        lastTime = time.clock()

        checkForQuit()
        tempx = getPlayerX()
        tempy = getPlayerY()

        for objects in GAMEOBJECTS:
            #print(type(objects), type(objects.x), type(objects.y), type(tempx), type(tempy))
            objects.x = objects.x - tempx
            objects.y = objects.y - tempy
            #print(type(objects))
            if 'scaled' not in objects.type:
                objects.size = int(objects.size*WINDOWHEIGHT/720)
                objects.type.append('scaled')
            if 'player' in objects.type:
                if clientID == objects.ownerID:
                    objects.iterate()
                    #print('iterating player')
            else:
                objects.iterate()
            objects.draw()
        pygame.display.update()
        FPSCLOCK.tick(FPS)

            # pygame.event.clear()

# threading solution discontinued. and send/recv solutiion with Process might be applicable to make simulation more constant.
def drawLoop():# # Discontinued
    stuartTime=time.clock()
    lastTime = time.clock()-1
    while True:
        if (time.clock()-stuartTime) > (1/FPS):
            print(1 / (time.clock() - lastTime))
            lastTime = time.clock()
            for objects in GAMEOBJECTS:
                objects.draw()
            pygame.display.update()
            stuartTime = stuartTime+1/FPS

        #FPSCLOCK.tick(FPS)

##################################################### A bunch of useful methods
def getPlayerX():
    for objects in GAMEOBJECTS:
        if 'player' in objects.type:
            if objects.ownerID ==clientID:
                return int(objects.x-WINDOWWIDTH/2)
    return 0

def getPlayerY():
    for objects in GAMEOBJECTS:
        if 'player' in objects.type:
            if objects.ownerID ==clientID:
                return int(objects.y-WINDOWHEIGHT/2)
    return 0

def setPlayerState(ownerID, x, y, hp): #Used by other clients to update player state
    for objects in GAMEOBJECTS:
        if 'player' in objects.type:
            if objects.ownerID == ownerID and ownerID != clientID:
                objects.x=x + WINDOWWIDTH/2 + bgx
                objects.y=y + WINDOWHEIGHT/2 + bgy
                objects.phealth=hp


functionWhiteList = {  # 'function': 'function',
                    '': '',
                    'player': 'player',
                    'basicEnemyZombie': 'basicEnemyZombie',
                    'basicEnemyProjectile': 'basicEnemyProjectile',
                    'setPlayerState': 'setPlayerState',
                    'playerProjectile': 'playerProjectile',
                    'playerAreaEffect': 'playerAreaEffect',
                    'playerExplosion': 'playerExplosion',
                    'setDirtBossState': 'setDirtBossState',
                    'dirtBossBombSetter': 'dirtBossBombSetter',
                    'dirtBossSwirlies': 'dirtBossSwirlies',
                    'dirtBossPoolDrop': 'dirtBossPoolDrop',
                    'dirtBossExplosion': 'dirtBossExplosion',
                    'GAMEOBJECTS[:] = []': 'GAMEOBJECTS[:] = []' ,
                    'background': 'background',
                    'dirtEnemy': 'dirtEnemy',
                    'basicEnemy': 'basicEnemy',
                    'broadcast': 'broadcast',
                    'mapTriggerSquare': 'mapTriggerSquare',
                    # Below here, unique codes that execute functions that can not be handled
                    # raw by handle(data)
                    'hopufhsoefy4089hfhjnapöiwhrf083rj2389fh8903hr': 'broadcast(\'player(ownerID = \' + str(clientID) + \')\')'
                    }

def handle(data): #Putting data handler function next to broadcast function, as they explain eachother.
    messList = data.split('##||')
    # Low effort security solution: will run only functions that are in the functionWhiteList
    # Dict, and will only read up to first ), do not know if this is vulnerable. 
    # Goal of this implementation is to not have to change the 36 uses of broadcast in the code
    [x.strip('##||') for x in messList]
    for x in messList:
        if("(" in x):
            parts = x.split("(")
            parts[1] = parts[1].split(")")
            #print("OH MY GOD ITS A FUNCCTION WTIH ARGS",parts)
            exec(functionWhiteList[parts[0]] + "(" + parts[1][0] + ")")
        else:
            #print("exec THIS biatch",x)
            exec(functionWhiteList[x])

def broadcast(broadcastData):# Data in is a string representing the code to be executed on all clients.
    broadcastData = broadcastData + '##||'
    # if broadcastData != '' and 'setPlayerState' not in broadcastData:
        # print('sending',broadcastData)
    # print('sending', broadcastData)
    broadcastData = bytes(broadcastData, "utf-8")
    # print('sending', broadcastData)
    server.sendall(broadcastData)
    # recieve()

def recieve(): #exclusively called by broadcast
    server.send(b'')
    data = server.recv(8092*3)
    try:
        data = data.decode("utf-8")
        handle(data)
    except EOFError:
        pass

def rotate(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.

    The angle should be given in radians.
    """
    ox, oy = origin
    px, py = point

    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return qx, qy

def translateCoordinates(x, y):

    return [str((x - bgx)/(WINDOWHEIGHT/720)) + '+ bgx', str((y - bgy)/(WINDOWHEIGHT/720)) + '+ bgy']

def terminate():
    pygame.quit()
    sys.exit()

def checkForQuit():
    for event in pygame.event.get(QUIT): # get all the QUIT events
        terminate() # terminate if any QUIT events are present
    for event in pygame.event.get(KEYUP): # get all the KEYUP events
        if event.key == K_ESCAPE:
            terminate() # terminate if the KEYUP event was for the Esc key
        pygame.event.post(event) # put the other KEYUP event objects back

##################################################### All-purpose classes
# Invisible wall loses to dash at 30 FPS, RIP
class RepeatedTimer:

    """Repeat `function` every `interval` seconds."""

    def __init__(self, interval, delay, function):
        self.interval = interval
        self.function = function
        #self.args = args
        #self.kwargs = kwargs
        self.delay=delay
        self.event = Event()
        self.thread = Thread(target=self._target)
        self.thread.start()

    def _target(self):
        while not self.event.wait(self._time):
            self.function()

    @property
    def _time(self):
        return self.interval - ((time.clock() - GLOBALTIME - self.delay) % self.interval)

    def stop(self):
        self.event.set()
        self.thread.join()

class invisibleWall:

    def iterate(self):
        for objects in GAMEOBJECTS:
            if 'collides' in objects.type:
                if abs(objects.x - self.x) < (self.dx+objects.size) and abs(objects.y - self.y) < (self.dy + objects.size):
                    if self.dx < self.dy:
                        print(1*((objects.x -self.x) < 0))
                        objects.x -= objects.pspeed / FPS * (((objects.x -self.x) < 0) - 0.5)*2
                    else:
                        objects.y -= objects.pspeed / FPS* (((objects.y -self.y) < 0)-0.5)*2
                        print(1*((objects.y -self.y) < 0))

    def draw(self):
        pygame.draw.circle(DISPLAYSURF, RED, (int(self.x), int(self.y)), 30)  # draw enemy

    def __init__(self, x, y, dx, dy):
        self.prevID = '0'
        self.type = ['invisiwall', 'collides']
        self.x = x
        self.y = y
        self.pspeed = 0
        self.size = 1
        self.dx = dx*self.size
        self.dy = dy*self.size
        self.type=[]
        GAMEOBJECTS.append(self)

class damageNumber(): # Can be used to draw any string, anywhere on screen, for any duration.
                        # Primary usage in name.
    def iterate(self):
        if (time.clock() - self.startTime) > self.duration:
            GAMEOBJECTS.remove(self)

    def draw(self):
        DISPLAYSURF.blit(self.image, (self.x, self.y))

    def __init__(self, x, y, text):
        self.tstart = time.clock()
        self.pspeed = 70 / FPS  # speed in pixels/second
        self.normspeed = self.pspeed
        self.x = x
        self.y = y
        self.duration = 2
        self.size=0
        self.color = 0
        if self.color + 2*int(text) <= 255:
            self.color = self.color + int(text)
        else:
            self.color = 255
        self.color = (self.color, 0, 255-self.color)  #needs better colors
        #self.font = pygame.font.Font(None, 10)
        self.image = font.render(str(text), True, self.color)
        self.startTime = time.clock()
        self.type = []
        GAMEOBJECTS.append(self)

class stringOnScreen():  # Can be used to draw any string, anywhere on screen, for any duration.
    def iterate(self):
        if (time.clock() - self.startTime) > self.duration:
            GAMEOBJECTS.remove(self)

    def draw(self):
        i=0
        for images in self.images:
            DISPLAYSURF.blit(images, (self.x, self.y+28*WINDOWHEIGHT/720*i))
            i=i+1

    def __init__(self, x, y, text, duration=2):
        self.tstart = time.clock()
        self.pspeed = 70 / FPS  # speed in pixels/second
        self.normspeed = self.pspeed
        self.x = x
        self.y = y
        self.duration = duration
        self.size = 0
        self.color = (200,200,200)
        texts = text.split('\n')
        #self.image = font.render('hej', True, self.color)
        self.images = []
        for lines in texts:
            self.images.append(textfont.render(lines, True, self.color))
        self.startTime = time.clock()
        self.type = []
        GAMEOBJECTS.append(self)

##################################################### The BasicEnemy set of classes
class basicEnemy(object):#Creating basic enemy

    def iterate(self):
        self.x = self.x + self.pvx  # simulate player position based on v, FPS
        self.y = self.y + self.pvy

        if self.phealth <1:
            #GAMEOBJECTS.remove(self)
            startGame()
        ###--------------------------------------------------------------------------------------------------------------------
        #  Collision signalling and detection, this code should be in all objects that can collide
        #  Consequently, all colliding objects need size and position variables
        #  Checks whether the type list of other objects initialized by the program contains "collides"
        #  If yes, runs a collision check
        for objects in GAMEOBJECTS:
            if 'collides' in objects.type and 'enemy' not in objects.type:
                if 'projectile' in objects.type:
                    pass
                elif ((self.x - objects.x))*((self.x - objects.x)) + ((self.y - objects.y))*((self.y - objects.y)) < ((self.size + objects.size))*((self.size + objects.size)):
                    self.pvx = self.pspeed*(self.x-objects.x)/abs(self.x-objects.x)/1.4/FPS
                    self.pvy = self.pspeed*(self.y-objects.y)/abs(self.y-objects.y)/1.4/FPS
                    self.x = self.x + self.pvx
                    self.y = self.y + self.pvy
                    if 'player' in objects.type:
                        objects.phealth = self.dealDamage(objects.phealth)

        if (time.clock()-self.startTime)%4  <=  1: #moving cycles every 4s
            self.movright()
        elif 1 < (time.clock()-self.startTime) %4 <= 2:
            self.movdown()
        elif 2 < (time.clock()-self.startTime) %4 <= 3:
            self.movleft()
        else:
            self.movup()


        if (time.clock()-self.startTime) %2 > 1: #Gonna use stuff like this a lot...
            if self.canShoot:
                self.shoot()
                self.canShoot = False
        else:
            self.canShoot=True

    def draw(self):
        if self.phealth != self.maxhealth:
            self.drawHP()
        pygame.draw.circle(DISPLAYSURF, GREEN, (int(self.x), int(self.y)), self.size)  # draw enemy

    def dealDamage(self, targethp):
        return(targethp-1)

    def drawHP(self):
        pygame.draw.rect(DISPLAYSURF, GREY, (self.x-self.size, self.y - self.size - 8, 2*self.size, 4))
        pygame.draw.rect(DISPLAYSURF, RED, (self.x-self.size, self.y - self.size - 8, 2*self.size*self.phealth/self.maxhealth, 4))

    def movup(self):
        self.pvy = - (self.pspeed / FPS)
        self.pvx = 0

    def movdown(self):
        self.pvy = + (self.pspeed / FPS)
        self.pvx = 0

    def movleft(self):
        self.pvx = - (self.pspeed / FPS)
        self.pvy = 0

    def movright(self):
        self.pvx =  (self.pspeed / FPS)  # enemy position as function of velocity in pixels per second, dividing by FPS gives the per-frame speed
        self.pvy = 0

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def shoot(self):
        for objects in GAMEOBJECTS:
            if 'player' in objects.type:
                #print(clientID, 'same as', objects.ownerID, '?')
                #print(clientID == objects.ownerID)
                if clientID == objects.ownerID:
                    [x, y] = translateCoordinates(self.x, self.y)
                    [ox, oy] = translateCoordinates(objects.x, objects.y)
                    broadcast('basicEnemyZombie(' + x + ', ' + y  +')')
                    broadcast('basicEnemyProjectile(' + x + ', ' + y + ', ' + ox + ', ' + oy +')')
    def __init__(self, x, y):
        #print('HELLO HELLO HELLO')
        self.x = x
        self.y = y
        self.pvleft = 0
        self.pvright= 0
        self.pvup= 0
        self.pvdown = 0
        self.pvx =0
        self.pvy=0
        players = []
        for objects in GAMEOBJECTS:
            if 'player' in objects.type:
                players.append(objects)
        self.maxhealth = 3000 * len(players)
        self.phealth = 3000 * len(players)
        self.size = 50
        self.pspeed = 45  # speed in pixels/second
        self.prevID = '0'
        self.type = ['boss', 'collides', 'pdamaging', 'enemy']
        self.canShoot = True
        self.startTime = time.clock()

        GAMEOBJECTS.append(self)
        # start timer

class basicEnemyProjectile(object):#Creating basic enemy

    def iterate(self):
        if time.clock() - self.tstart > 10:
            if self in GAMEOBJECTS:
                GAMEOBJECTS.remove(self)
        self.pvx = self.pvx * self.pspeed/self.normspeed
        self.pvy = self.pvy * self.pspeed/self.normspeed
        self.x = self.x + self.pvx  # simulate position
        self.y = self.y + self.pvy

        ###--------------------------------------------------------------------------------------------------------------------
        #  Collision signalling and detection, this code should be in all objects that can collide
        #  Consequently, all colliding objects need size and position variables
        #  Checks whether the type list of other objects initialized by the program contains "collides"
        #  If yes, runs a collision check
        for objects in GAMEOBJECTS:
            if 'collides' in objects.type and self.x != objects.x and self.y != objects.y and 'player' in objects.type:
                if ((self.x - objects.x))*((self.x - objects.x)) + ((self.y - objects.y))*((self.y - objects.y)) < ((self.size + objects.size))*((self.size + objects.size)):
                    self.pvx = self.pspeed*(self.x-objects.x)/abs(self.x-objects.x)/1.4/FPS
                    self.pvy = self.pspeed*(self.y-objects.y)/abs(self.y-objects.y)/1.4/FPS
                    self.x = self.x + self.pvx
                    self.y = self.y + self.pvy
                    objects.phealth = self.dealDamage(objects.phealth)

    def draw(self):
        pygame.draw.circle(DISPLAYSURF, GREEN, (int(self.x), int(self.y)), self.size)  # draw enemy projectile

    def dealDamage(self, targethp):
        if self in GAMEOBJECTS:
            GAMEOBJECTS.remove(self)
        return (targethp - 10)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __init__(self, x, y, tx, ty):
        #print('HELLO HELLO HELLO')
        self.tstart = time.clock()
        self.pspeed = 70/FPS  # speed in pixels/second
        self.normspeed = self.pspeed
        self.pvx = (tx-x)/math.sqrt((tx-x)*(tx-x) + (ty-y)*(ty-y))*self.pspeed
        self.pvy = (ty-y)/math.sqrt((tx-x)*(tx-x) + (ty-y)*(ty-y))*self.pspeed
        self.x = x
        self.y = y
        self.size = 5
        self.prevID = '0'
        self.type = ['collides', 'pdamaging', 'projectile']
        GAMEOBJECTS.append(self)

        # start timer

class basicEnemyZombie(object):#Creating basic enemy

    def iterate(self):
        self.x = self.x + self.pvx  # simulate player position based on v, FPS
        self.y = self.y + self.pvy

        if self.phealth <1:
            GAMEOBJECTS.remove(self)
        ###--------------------------------------------------------------------------------------------------------------------
        #  Collision signalling and detection, this code should be in all objects that can collide
        #  Consequently, all colliding objects need size and position variables
        #  Checks whether the type list of other objects initialized by the program contains "collides"
        #  If yes, runs a collision check
        for objects in GAMEOBJECTS:
            if 'player' in objects.type:
                #print('comparing', objects.ownerID, self.targetID)
                if objects.ownerID == self.targetID:
                    #print(self.pvx, self.pvy, '-------------- BEFORE')
                    self.pvx = self.pspeed * (objects.x-self.x) / abs(self.x - objects.x + 0.01) / 1.4 / FPS
                    self.pvy = self.pspeed * (objects.y-self.y) / abs(self.y - objects.y + 0.01) / 1.4 / FPS
                    #print(self.pvx, self.pvy, '-------------- AFTER')
            if 'collides' in objects.type and 'enemy' not in objects.type:
                if 'projectile' in objects.type:
                    pass
                elif ((self.x - objects.x))*((self.x - objects.x)) + ((self.y - objects.y))*((self.y - objects.y)) < ((self.size + objects.size))*((self.size + objects.size)):
                    self.pvx = self.pspeed*(self.x-objects.x)/abs(self.x-objects.x)/1.4/FPS
                    self.pvy = self.pspeed*(self.y-objects.y)/abs(self.y-objects.y)/1.4/FPS
                    self.x = self.x + self.pvx
                    self.y = self.y + self.pvy
                    if 'player' in objects.type:
                        objects.phealth = self.dealDamage(objects.phealth)

        if (time.clock()-self.startTime) %2 > 1: #Gonna use stuff like this a lot...
            if self.canShoot:
                self.pspeed += 5
                self.canShoot = False
        else:
            self.canShoot=True

    def draw(self):
        pygame.draw.circle(DISPLAYSURF, GREEN, (int(self.x), int(self.y)), self.size)  # draw enemy
        if self.phealth != self.maxhealth:
            self.drawHP()

    def dealDamage(self, targethp):
        return(targethp-3)

    def drawHP(self):
        pygame.draw.rect(DISPLAYSURF, GREY, (self.x-self.size, self.y - self.size - 8, 2*self.size, 4))
        pygame.draw.rect(DISPLAYSURF, RED, (self.x-self.size, self.y - self.size - 8, 2*self.size*self.phealth/self.maxhealth, 4))


    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def shoot(self):
        for objects in GAMEOBJECTS:
            if 'player' in objects.type:
                #print(clientID, 'same as', objects.ownerID, '?')
                #print(clientID == objects.ownerID)
                if clientID == objects.ownerID:
                    [x, y] = translateCoordinates(self.x, self.y)
                    [ox, oy] = translateCoordinates(objects.x, objects.y)
                    broadcast('basicEnemyProjectile(' + x + ', ' + y + ', ' + ox + ', ' + oy +')')
    def __init__(self, x, y):
        #print('HELLO HELLO HELLO')
        self.x = x
        self.y = y
        self.pvx =0
        self.pvy=0
        self.size = 10
        self.pspeed = 25  # speed in pixels/second
        self.prevID = '0'
        self.type = ['collides', 'pdamaging', 'enemy']
        self.canShoot = True
        self.startTime = time.clock()
        players = []
        for objects in GAMEOBJECTS:
            if 'player' in objects.type:
                players.append(objects)
        self.targetID = random.choice(players).ownerID
        self.maxhealth = int(160*math.sqrt(len(players)))
        self.phealth = self.maxhealth
        GAMEOBJECTS.append(self)
        # start timer

##################################################### Player classes
class player(object):# Creating the player
    def iterate(self): #listener loop, listens for events from main and calls player
        #### Regen health
        if time.clock()-self.hpTime > 2:
            self.hpTime += 1
            if self.phealth < self.maxhp:
                self.phealth += 1
        ##############################################################################################player movement logic
        for event in pygame.event.get(KEYDOWN):  # get all the KEYUP events
            if event.key == K_w:
                self.pvup = - (self.pspeed / FPS)  # gives player upward velocity
            if event.key == K_a:
                self.pvleft =  - (self.pspeed / FPS)  # gives player leftward velocity
            if event.key == K_s:
                self.pvdown =  (self.pspeed / FPS)  # gives player downward velocity
            if event.key == K_d:
                self.pvright =  (self.pspeed / FPS)  # gives player rightward velocity
            if event.key == K_LSHIFT:
                castingPanel()
            if event.key == K_g:
                self.resetGame()
        for event in pygame.event.get(KEYUP):
            if event.key == K_w:
                self.pvup = 0  # stop movement on key release
            if event.key == K_a:
                self.pvleft = 0  #
            if event.key == K_s:
                self.pvdown = 0  #
            if event.key == K_d:
                self.pvright = 0  #
            if event.key == K_LSHIFT :
                for objects in GAMEOBJECTS:
                    if 'castingPanel' in objects.type:
                        #print('awbduoabtwfo')
                        objects.castSpell()
                        objects.destruct()
        self.pvy = self.pvup + self.pvdown
        self.pvx = self.pvleft + self.pvright
        #print('bgdrawn and enemy drawn')
        predicate = self.pvy * self.pvy + self.pvx * self.pvx > ((1.05 * self.pspeed / FPS)) * (
        (1.05 * self.pspeed / FPS))
        if predicate:
            # print('zooommm')
            self.pvy = self.pvy / 1.4
            self.pvx = self.pvx / 1.4

        self.x = self.x + self.pvx  # simulate player position based on v, FPS
        self.y = self.y + self.pvy
        ###--------------------------------------------------------------------------------------------------------------------
        #  Collision signalling and detection, this code should be in all objects that can collide
        #  Consequently, all colliding objects need size and position variables
        #e = pygame.event.Event(USEREVENT + 1, denomination='collide', x=self.x, y=self.y, size=self.size)
        #pygame.event.post(e)
        for objects in GAMEOBJECTS:
            if 'collides' in objects.type and self.x != objects.x and self.y != objects.y and 'player' not in objects.type:
                if ((self.x - objects.x))*((self.x - objects.x)) + ((self.y - objects.y))*((self.y - objects.y)) < ((self.size + objects.size))*((self.size + objects.size)):
                    self.pvx = self.pspeed*(self.x-objects.x)/abs(self.x-objects.x)/1.4/FPS
                    self.pvy = self.pspeed*(self.y-objects.y)/abs(self.y-objects.y)/1.4/FPS
                    self.x = self.x + self.pvx
                    self.y = self.y + self.pvy

        #if time.clock() % 0.3 > 0.1:
        #    if self.shouldBroadcast:
        #        self.broadcastState()
        #        self.shouldBroadcast = False
        #if time.clock() % 2 < 1:
        #    self.shouldBroadcast = True
        self.broadcastState()
        #if self in GAMEOBJECTS:
        #    GAMEOBJECTS.remove(self)
        #    GAMEOBJECTS.append(self)
        if self.phealth < 0:
            self.resetGame()


    def broadcastState(self): #Called by recieve?
        broadcast('setPlayerState(' + str(self.ownerID) + ', ' + str(- bgx) + ', ' + str(- bgy) + ', ' + str(self.phealth) + ')')

    def draw(self):
        pygame.draw.circle(DISPLAYSURF, WHITE, (int(self.x), int(self.y)), self.size)  # draw player
        if self.ownerID == clientID:
            pygame.draw.rect(DISPLAYSURF, GREY, (WINDOWWIDTH/4,  WINDOWHEIGHT-20, WINDOWWIDTH/2, 20))
            pygame.draw.rect(DISPLAYSURF, RED, (WINDOWWIDTH/4, WINDOWHEIGHT-20, WINDOWWIDTH/2*self.phealth/self.maxhp, 20))
        else:
            pygame.draw.rect(DISPLAYSURF, GREY, (self.x - self.size, self.y - self.size - 8, 2 * self.size, 4))
            pygame.draw.rect(DISPLAYSURF, RED, (
            self.x - self.size, self.y - self.size - 8, 2 * self.size * self.phealth / self.maxhp, 4))

    def resetGame(self):
        startGame()

    def __init__(self, ownerID = clientID):
        self.x = int(WINDOWWIDTH/2)
        self.y = int(WINDOWHEIGHT/2)
        self.pvleft = 0
        self.pvright= 0
        self.pvup= 0
        self.pvdown = 0
        self.pvx =0
        self.pvy=0
        self.maxhp=100
        self.phealth = 100
        self.size = 15
        self.pspeed = 210  # speed in pixels/second
        self.prevID = '0'
        self.type = ['collides', 'player'] #objects that collide must have size and position, x, y, z variables
        self.ownerID = ownerID
        self.hpTime=time.clock()
        print(ownerID)
        self.shouldBroadcast=True
        GAMEOBJECTS.append(self)

class castingPanel(object):

    def destruct(self):
        GAMEOBJECTS.remove(self)

    def draw(self):
        if WINDOWWIDTH <= WINDOWHEIGHT:
            self.localSide = WINDOWWIDTH/2
        else:
            self.localSide = WINDOWHEIGHT/2
        #pygame.draw.rect(DISPLAYSURF, BLACK, ((WINDOWWIDTH/2-self.localSide/2, (WINDOWHEIGHT/2-self.localSide/2, self.localSide, self.localSide))
         #draw grid on surface
        pygame.draw.line(DISPLAYSURF, GREY, ((WINDOWWIDTH/2)-self.localSide/2+self.localSide/3, (WINDOWHEIGHT/2)-self.localSide/2),
                         ((WINDOWWIDTH/2)-self.localSide/2+self.localSide/3, (WINDOWHEIGHT)/2+self.localSide/2), 4)
        pygame.draw.line(DISPLAYSURF, GREY, ( (WINDOWWIDTH/2) - self.localSide / 2 + 2*self.localSide / 3, (WINDOWHEIGHT/2) - self.localSide / 2),
                         ((WINDOWWIDTH/2) - self.localSide / 2 + 2* self.localSide / 3, (WINDOWHEIGHT/2) + self.localSide / 2), 4)
        pygame.draw.line(DISPLAYSURF, GREY, ((WINDOWWIDTH/2) - self.localSide / 2, (WINDOWHEIGHT/2) - self.localSide / 2 + self.localSide / 3),
                         ((WINDOWWIDTH/2) + self.localSide / 2, (WINDOWHEIGHT/2) - self.localSide / 2 +self.localSide / 3), 4)
        pygame.draw.line(DISPLAYSURF, GREY, ((WINDOWWIDTH/2) - self.localSide / 2, (WINDOWHEIGHT/2) - self.localSide / 2 + 2* self.localSide / 3),
                         ((WINDOWWIDTH/2) + self.localSide / 2, (WINDOWHEIGHT/2) - self.localSide / 2 + 2*self.localSide / 3), 4)

    def iterate(self):
        #print(pygame.mouse.get_pressed())
        self.lastpos=self.pos
        self.pos = pygame.mouse.get_pos()
        self.x = (WINDOWWIDTH/2)
        self.y = (WINDOWHEIGHT/2)
        if pygame.mouse.get_pressed()[0]: #check if left mouse button is pressed (true when pressed)
            self.drawing = True
            if self.lastpos == 'not initiated':
                self.linePoints.append(self.pos)
            else:
                self.linePoints.append(self.pos)
                tmp = 'nope'
                for values in self.linePoints:
                    if tmp == 'nope':
                        tmp = values
                        continue
                    pygame.draw.line(DISPLAYSURF, RED,(tmp),(values), 4)
                    tmp = values
        else:
            self.lastpos = 'not initiated'
            if self.drawing == True:  #Put code for finishing a glyph here
                self.addRune(self.linePoints)
                self.linePoints = []
                self.drawing = False

        GAMEOBJECTS.remove(self)
        GAMEOBJECTS.append(self) # makes sure it is always drawn last

    def spellbook(self, start, end, points): #spells are assigned to matrixelements.
        starts = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        starts2 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        starts3 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        starts4 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        starts5 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        starts6 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        starts7 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        starts8 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        starts9 = [1, 2, 3, 4, 5, 6, 7, 8, 9]

        start = start-1
        end = end-1
        #print(starts) #deduct 1 from square numbers in this code (fml)
        startends = [starts, starts2, starts3, starts4, starts5, starts6, starts7, starts8, starts9]
        startends[0][6] = 'pFireball'
        startends[6][8] = 'pPowerUp'
        startends[1][1] = 'blastUp'
        startends[2][8] = 'multicast'
        startends[7][5] = 'dash'
        startends[0][8] = 'blizzard'
        startends[2][6] = 'CWDT'
        if len(self.badness) < 6: #maximum runes in a spell is 5
            self.badness.append(self.checkBadness(startends[start][end], points))
        self.runeList.append(startends[start][end])

    def checkBadness(self, rune, drawing):
        #rune.append('()')
        #correctPoints = exec(rune)   #Started a sketch of method
        #distBetweenPoints = []
        #sumOfDists = []
        #for i in correctPoints[0]:
        #    if i < 1:
        #        distBetweenPoints.append(0)
        #        sumOfDists.append(sum(distBetweenPoints))
        #    else:
        #        expr = (correctPoints[0][i]-correctPoints[0][i-1])**2+(correctPoints[1][i]-correctPoints[1][i-1])**2 #Calculate distance between points
        #        distBetweenPoints.append(expr)
        #        sumOfDists.append(sum(distBetweenPoints))


        #lines = [line.rstrip('\n') for line in open(rune)]
        #sqError = 0:
        #for points in drawing:
        #   for elements in lines:

        return 0  #Goal is to make this something betweeon 0 and 1, 0 good.


    def castSpell(self):
        playerCastSpell(self.runeList, self.badness)

    def addRune(self, points): #a bunch of if clauses to identify runes and their quality. Quality asessment is its own method

        #Classify runes based on which square of the grid start-and endpoints are in
        runeStart = points[0]
        runeEnd = points[-1]
        glyphStart = 1
        glyphEnd = 3
        #define top left corner of runeboard:
        xOrigin = self.x-self.localSide/2
        yOrigin = self.y-self.localSide/2
        pygame.draw.line(DISPLAYSURF, BLUE, (xOrigin, yOrigin), (xOrigin+4, yOrigin), 4)
        localSide=self.localSide
        for point in [runeStart, runeEnd]:
            if point[0] <= xOrigin + localSide/3 and point[1] <= yOrigin + localSide/3: #if x and y coords in first square, set value to 1. Order is reading order.
                if (point[0], point[1]) == runeStart:
                    glyphStart = 1
                elif (point[0], point[1]) == runeEnd:
                    glyphEnd = 1
            elif xOrigin + localSide/3 < point[0] <= xOrigin + 2*localSide/3 and point[1] <= yOrigin + localSide/3:# logic for sq 2
                if (point[0], point[1]) == runeStart:
                    glyphStart = 2
                elif (point[0], point[1]) == runeEnd:
                    glyphEnd = 2
            elif xOrigin + 2*localSide/3 < point[0] and point[1] <= yOrigin + localSide/3: # should be third
                if (point[0], point[1]) == runeStart:
                    glyphStart = 3
                elif (point[0], point[1]) == runeEnd:
                    glyphEnd = 3
            elif point[0] <= xOrigin + localSide/3 and yOrigin + localSide/3 < point[1] <= yOrigin + 2*localSide/3: # should be fourth
                if (point[0], point[1]) == runeStart:
                    glyphStart = 4
                elif (point[0], point[1]) == runeEnd:
                    glyphEnd = 4
            elif xOrigin + localSide/3 < point[0] <= xOrigin + 2*localSide/3 and yOrigin + localSide/3 < point[1] <= yOrigin + 2*localSide/5: # should be fifth
                if (point[0], point[1]) == runeStart:
                    glyphStart = 5
                elif (point[0], point[1]) == runeEnd:
                    glyphEnd = 5
            elif xOrigin + 2*localSide/3 < point[0] and yOrigin + localSide/3 < point[1] <= yOrigin + 2*localSide/3: # should be sixth
                if (point[0], point[1]) == runeStart:
                    glyphStart = 6
                elif (point[0], point[1]) == runeEnd:
                    glyphEnd = 6
            elif point[0] <= xOrigin + localSide/3 and yOrigin + 2*localSide/3 < point[1]:# should be seventh
                if (point[0], point[1]) == runeStart:
                    glyphStart = 7
                elif (point[0], point[1]) == runeEnd:
                    glyphEnd = 7
            elif xOrigin + localSide/3 < point[0] <= xOrigin + 2*localSide/3 and yOrigin + 2*localSide/3 < point[1]:# should be eighth
                if (point[0], point[1]) == runeStart:
                    glyphStart = 8
                elif (point[0], point[1]) == runeEnd:
                    glyphEnd = 8
            else:# should be ninth
                if (point[0], point[1]) == runeStart:
                    glyphStart = 9
                elif (point[0], point[1]) == runeEnd:
                    glyphEnd = 9
        #one if clause per rune that does something
        self.spellbook(glyphStart, glyphEnd, self.linePoints)

    def __init__(self):
        self.x = int(WINDOWWIDTH/2)
        self.y = int(WINDOWHEIGHT/2)
        self.size = 15
        self.pspeed = 120  # speed in pixels/second
        self.prevID = '0'
        self.type = ['castingPanel'] #objects that collide must have size and position, x, y, z variables
        self.pos = 'not initiated'
        self.linePoints = []
        self.drawing = False
        self.castComplete = False
        self.runeList = []
        self.badness = []
        GAMEOBJECTS.append(self)

class playerCastSpell(object): #Think about main spell types: projectile from player, blast on reticle, blast on self, teleport can have own code, lasting area effect, walls

    def iterate(self):
        if 'CWDT' in self.type:
            self.runeList.remove('CWDT')
            playerEffect(runeList = self.runeList, badness = self.badness[:-1], charges=10, type=['CWDT'])
            GAMEOBJECTS.remove(self)
        if 'projectile' in self.type:
            if self.casted == False: #also needs to be a projectile for this to make sense
                if pygame.mouse.get_pressed()[0]:
                    self.x = (WINDOWWIDTH / 2)
                    self.y = (WINDOWHEIGHT / 2)
                    self.casted = True
                    [x, y] = translateCoordinates(self.x, self.y)
                    tarX = pygame.mouse.get_pos()[0]
                    tarY = pygame.mouse.get_pos()[1]
                    [tarx, tary] = translateCoordinates(tarX, tarY)
                    if 'multicast' in self.type:
                        #print('entering multicast code')
                        for i in range(0, self.nOSpells):
                            print(i, self.nOSpells, 'and' , str(self.nOSpells/2 - i - 0.5))
                            [tarXX, tarYY] = rotate(   (self.x, self.y), (tarX, tarY), math.pi/6*(self.nOSpells/2 - i) )
                            [tarx, tary] = translateCoordinates(tarXX, tarYY)
                            broadcast('playerProjectile(' + str(
                                self.type) + ',' + x + ',' + y + ',' + tarx + ',' + tary + ',' + str(
                                self.damage) + ',' + str(self.exploDMG) + ',' + str(self.blastRad) + ')')
                    else:
                        broadcast('playerProjectile(' + str(self.type) + ',' + x + ',' + y + ',' + tarx + ',' + tary + ',' + str(self.damage) + ',' + str(self.exploDMG) + ',' + str(self.blastRad) + ')')
                elif self.autoCasted:
                    self.x = (WINDOWWIDTH / 2)
                    self.y = (WINDOWHEIGHT / 2)
                    self.casted = True
                    [x, y] = translateCoordinates(self.x, self.y)
                    tarX = self.tarx
                    tarY = self.tary
                    [tarx, tary] = translateCoordinates(tarX, tarY)
                    if 'multicast' in self.type:
                        # print('entering multicast code')
                        for i in range(0, self.nOSpells):
                            print(i, self.nOSpells, 'and', str(self.nOSpells / 2 - i - 0.5))
                            [tarXX, tarYY] = rotate((self.x, self.y), (tarX, tarY),
                                                    math.pi / 6 * (self.nOSpells / 2 - i))
                            [tarx, tary] = translateCoordinates(tarXX, tarYY)
                            broadcast('playerProjectile(' + str(
                                self.type) + ',' + x + ',' + y + ',' + tarx + ',' + tary + ',' + str(
                                self.damage) + ',' + str(self.exploDMG) + ',' + str(self.blastRad) + ')')
                    else:
                        broadcast('playerProjectile(' + str(
                            self.type) + ',' + x + ',' + y + ',' + tarx + ',' + tary + ',' + str(
                            self.damage) + ',' + str(self.exploDMG) + ',' + str(self.blastRad) + ')')

        if 'reticleBlast' in self.type:
            if pygame.mouse.get_pressed()[0]:
                tarx1 = pygame.mouse.get_pos()[0]
                tary1 = pygame.mouse.get_pos()[1]
                [tarx, tary] = translateCoordinates(tarx1, tary1)
                if 'areaEffect' in self.type:
                    #print('areaEffect works')
                    broadcast('playerAreaEffect(' + tarx + ',' + tary + ',' + str(self.exploDMG) + ',' + str(
                        self.blastRad) + ',' + str(self.spellColor) + ',' + str(self.type) +  ',' + str(self.duration) + ',' + str(self.tickrate) +')')
                else:
                    broadcast('playerExplosion(' + tarx + ',' + tary + ',' + str(self.exploDMG) + ',' + str(self.blastRad) + ',' + str(self.spellColor) + ',' + str(self.type) + ')')
                if self in GAMEOBJECTS:
                    GAMEOBJECTS.remove(self)
            elif self.autoCasted:
                tarx1 = self.tarx
                tary1 = self.tary
                [tarx, tary] = translateCoordinates(tarx1, tary1)
                if 'areaEffect' in self.type:
                    # print('areaEffect works')
                    broadcast('playerAreaEffect(' + tarx + ',' + tary + ',' + str(self.exploDMG) + ',' + str(
                        self.blastRad) + ',' + str(self.spellColor) + ',' + str(self.type) + ',' + str(
                        self.duration) + ',' + str(self.tickrate) + ')')
                else:
                    broadcast('playerExplosion(' + tarx + ',' + tary + ',' + str(self.exploDMG) + ',' + str(
                        self.blastRad) + ',' + str(self.spellColor) + ',' + str(self.type) + ')')
                GAMEOBJECTS.remove(self)

        if 'dash' in self.type:
            if pygame.mouse.get_pressed()[0]:
                tarx1 = pygame.mouse.get_pos()[0]
                tary1 = pygame.mouse.get_pos()[1]
                for objects in GAMEOBJECTS:
                    if 'player' in objects.type: #Player effects can be run locally?
                        if objects.ownerID == clientID:
                            dirX = (tarx1 - objects.x)/( (tarx1 - objects.x)**2 + (tary1 - objects.y)**2  )
                            dirY = (tary1 - objects.y)/( (tarx1 - objects.x)**2 + (tary1 - objects.y)**2  )
                            direction = [dirX, dirY];
                            playerEffect(direction=direction, magnitude=self.damage, type=['dash'], target = objects)
                GAMEOBJECTS.remove(self)
            elif self.autoCasted:
                tarx1 = self.tarx
                tary1 = self.tary
                for objects in GAMEOBJECTS:
                    if 'player' in objects.type:  # Player effects can be run locally?
                        if objects.ownerID == clientID:
                            dirX = (tarx1 - objects.x) / ((tarx1 - objects.x) ** 2 + (tary1 - objects.y) ** 2)
                            dirY = (tary1 - objects.y) / ((tarx1 - objects.x) ** 2 + (tary1 - objects.y) ** 2)
                            direction = [dirX, dirY];
                            playerEffect(direction=direction, magnitude=self.damage, type=['dash'], target=objects)
                GAMEOBJECTS.remove(self)


    def specifySpell(self, runeList, badness): #in future, probably make this its own file, cuz jesus
        if runeList:
            if 'pFireball' == runeList[0]: #First rune must be a a primary rune (corresponding to an actual spell. Support runes are always applied afterwards and in sequence
                self.x = (WINDOWWIDTH/2)
                self.y = (WINDOWHEIGHT/2)
                self.size = 15
                self.pspeed = 240
                self.type.append('explodes') #explodes for 50% of main blast damage
                self.type.append('projectile')
                self.damage = 10*(1-0.3*badness[0])
                self.exploDMG = 5
                self.blastRad = 60
                #print('casting fireball')
                self.spellColor = RED
            if 'dash' == runeList[0]:
                self.type.append('dash')
                self.exploDMG = 1
                self.damage = 10000
                self.blastRad = 60
                self.range=150
                self.spellColor=BLUE
            if 'blizzard' == runeList[0]:
                self.type.append('reticleBlast')
                self.type.append('areaEffect')
                self.exploDMG = 1
                self.blastRad = 50
                self.range = 150
                self.tickrate = 0.5
                self.duration = 5
                self.spellColor = BLUE
            else:
                pass#GAMEOBJECTS.remove(self)
            for i in range(1,len(runeList)): #in this for loop, all support runes are applied.
                if 'pPowerUp' == runeList[i]:
                    self.damage = int((2.3-0.8*badness[i])*self.damage)
                    self.exploDMG = int((2.3-0.8*badness[i])*self.exploDMG)
                    #print('powering up!')
                    #print(self.damage)
                if 'blastUp' == runeList[i]:
                    self.blastRad = self.blastRad*(1.5-0.3*badness[i])
                    self.exploDMG = int((2-0.8*badness[i])*self.exploDMG)
                    self.type.append('explodes')
                    #print(self.blastRad)
                if 'multicast' == runeList[i]:
                    self.nOSpells *= 3 - (1*(badness[i]>0.5))
                    self.type.append('multicast')
                if 'CWDT' == runeList[i]:
                    self.type.append('CWDT')

        else:
            if self in GAMEOBJECTS:
                GAMEOBJECTS.remove(self)
    def draw(self):
        pass

    def __init__(self, runeList, badness, tarx='nope', tary='nope'):
        self.autoCasted = False
        self.x = int(WINDOWWIDTH/2)
        self.y = int(WINDOWHEIGHT/2)
        self.pvy = 0
        self.pvx = 0
        self.nOSpells = 1
        self.size = 10     # all spells should overwrite base size
        self.pspeed = 100  # speed in pixels/second
        self.type = ['playerSpell'] #objects that collide must have size and position, x, y, z variables
        if len(badness)>5:
            self.badness=badness[0:5]
        else:
            self.badness = badness
        if len(runeList)>5:
            self.runeList=runeList[0:5]
        else:
            self.runeList = runeList
        self.damage = 0
        self.exploDMG = 0
        self.blastRad = 0
        self.casted = False
        self.spellColor = RED
        self.range=0
        print(self.runeList, self.badness)
        self.specifySpell(self.runeList, self.badness)
        if tarx != 'nope' and tary != 'nope':
            self.tarx = tarx
            self.tary = tary
            self.autoCasted = True
        else:
            self.tarx = 0
            self.tary = 0

        GAMEOBJECTS.append(self)

class playerEffect(object):

    def draw(self): #put eventual FX here!
        pass

    def iterate(self):
        if self.target != 'nope' and  'dash' in self.type:
            print('--------------dashin!')
            self.target.x += self.direction[0]*self.magnitude/FPS
            self.target.y += self.direction[1]*self.magnitude/FPS
            if (time.clock()-self.starttime) > self.duration:
                print('slowing down')
                GAMEOBJECTS.remove(self)
            #print(self.direction[0]*self.magnitude/FPS, self.direction[1]*self.magnitude/FPS)
        if 'CWDT' in self.type:
            #print('CWDT in type!')
            if (time.clock()-self.starttime)>0.25 and self.target.phealth < self.startHP:
                tarx = (random.random()-0.5)*240 + WINDOWWIDTH/2
                tary = (random.random()-0.5)*240 + WINDOWHEIGHT/2
                print('casting',self.runeList, self.badness, tarx, tary)
                playerCastSpell(self.runeList, self.badness, tarx=tarx, tary=tary) #playercastSpell already normalizes
                self.starttime = time.clock()
                self.charges = self.charges-1
                if self.charges < 0:
                    GAMEOBJECTS.remove(self)
            self.startHP = self.target.phealth

    def __init__(self, type=[], x=0, y=0, tarx=0, tary=0, charges=0, runeList=[], damage=0, badness=[],
                 magnitude=0, direction =[], duration=0.4, color=RED, target = 'nope'):  # add color specification some day =)
        print('playerEffect GO!')
        self.x = x
        self.y = y
        self.runeList = runeList
        self.badness = badness
        self.charges=charges
        self.size = 0
        self.duration = duration
        self.pspeed = 100
        #self.type = ['playerSpell']
        self.type = type
        #print(str(self.type), 'is the types the projectile has')
        self.damage = damage
        self.casted = False
        self.starttime = time.clock()
        self.spellColor = color
        self.magnitude = magnitude
        #self.color = color
        self.target = target
        self.direction = direction
        if target == 'nope':
            for objects in GAMEOBJECTS:
                if 'player' in objects.type:
                    if objects.ownerID == clientID:
                        self.target = objects
        if 'CWDT' in self.type:
            self.startHP = self.target.phealth
        if 'dash' in self.type:
            self.magnitude = math.sqrt(self.magnitude)*480
        # print('itsabomb!')
        GAMEOBJECTS.append(self)

class playerProjectile(object):

    def iterate(self):
        self.x = (self.x + self.pvx)  # simulateaaaaaaaaaaaa
        self.y = (self.y + self.pvy)
        ###--------------------------------------------------------------------------------------------------------------------
        #  Collision signalling and detection, this code should be in all objects that can collide
        #  Consequently, all colliding objects need size and position variables
        #  Checks whether the type list of other objects initialized by the program contains "collides"
        #  If yes, runs a collision check
        for objects in GAMEOBJECTS:
            if 'enemy' in objects.type:
                if ((self.x - objects.x)) * ((self.x - objects.x)) + ((self.y - objects.y)) * ((self.y - objects.y)) < (
                (self.size + objects.size)) * ((self.size + objects.size)):  # what the fuck
                    objects.phealth -= self.damage
                    damageNumber(objects.x, objects.y - objects.size - 10, self.damage)
                    print('spell deals ', self.damage, 'damage')
                    if 'explodes' in self.type:
                        print('explosion should deal ',self.exploDMG, 'damage')
                        playerExplosion(self.x, self.y, self.exploDMG, self.blastRad, self.spellColor)
                    if self in GAMEOBJECTS:
                        GAMEOBJECTS.remove(self)

    def draw(self):
        pygame.draw.circle(DISPLAYSURF, self.spellColor, (int(self.x), int(self.y)), self.size)  # draw spell

    def __init__(self, type, x, y, tarx, tary, damage=10, exploDMG=10, blastRad=10, color=RED):  # add color specification some day =)
        self.x = x
        self.y = y
        self.size = 10
        self.pspeed = 100
        #self.type = ['playerSpell']
        self.type = type
        #print(str(self.type), 'is the types the projectile has')
        self.damage = damage
        self.exploDMG = exploDMG
        self.casted = False
        self.starttime = time.clock()
        self.blastRad = blastRad
        self.spellColor = color
        #self.color = color

        self.pvx = self.pspeed * (tarx - self.x) / math.sqrt(
            (tarx - self.x) * (tarx - self.x) + (tary - self.y) * (tary - self.y)) / FPS
        self.pvy = self.pspeed * (tary - self.y) / math.sqrt(
            (tarx - self.x) * (tarx - self.x) + (tary - self.y) * (tary - self.y)) / FPS
        # print('itsabomb!')
        GAMEOBJECTS.append(self)

class playerExplosion(object):

    def iterate(self):
        if time.clock()-self.starttime < 1:
            self.size = int(self.blastRad*(time.clock()-self.starttime))
            for objects in GAMEOBJECTS:
                if 'enemy' in objects.type:
                    if ((self.x - objects.x)) * ((self.x - objects.x)) + ((self.y - objects.y)) * (
                    (self.y - objects.y)) < ((self.size + objects.size)) * (
                    (self.size + objects.size)):  # what the fuck
                        if 'knockback' in self.type:
                            print('knocking back')
                            objects.x += (objects.x-self.x)/math.sqrt((objects.x-self.x)*(objects.x-self.x) + (objects.y-self.y)*(objects.y-self.y))*100/FPS
                            objects.x += (objects.x - self.x) / math.sqrt((objects.x - self.x) * (objects.x - self.x) + (objects.y - self.y) * (objects.y - self.y)) * 100 / FPS
                        if self.starttime not in objects.type:
                            objects.phealth -= int(self.damage)
                            damageNumber(objects.x, objects.y - objects.size - 10, self.damage)
                            print('explosion deals ', self.damage, 'damage')
                            objects.type.append(self.starttime)
        else:
            GAMEOBJECTS.remove(self)

    def draw(self):
        pygame.draw.circle(DISPLAYSURF, self.color, (int(self.x), int(self.y)), self.size, 4*(4<self.size))

    def __init__(self, x, y, damage, blastRad = 60, color=RED, type = []): #add color specification some day =)
        self.x = x
        self.y = y
        self.size = 0
        self.pspeed = 100
        #self.type = ['playerSpell']
        self.type = type
        self.damage = damage
        self.casted = False
        self.starttime = time.clock()
        self.blastRad = blastRad
        self.color=color
        print(color)
        #print('itsabomb!')
        GAMEOBJECTS.append(self)

class playerAreaEffect(object):

    def iterate(self):
        #print('blizzard iterating')
        if time.clock() - self.starttime < self.duration:
            self.size = int(self.blastRad*(1 + 0.03*math.sin(5*(time.clock() - self.starttime))))
            self.didDamage = False
            for objects in GAMEOBJECTS:
                if 'enemy' in objects.type:
                    if ((self.x - objects.x)) * ((self.x - objects.x)) + ((self.y - objects.y)) * (
                            (self.y - objects.y)) < ((self.size + objects.size)) * (
                            (self.size + objects.size)):  # what the fuck
                        if (time.clock() - self.starttime)%self.tickrate < self.tickrate/2:
                            self.canDamage=True
                        elif self.canDamage:
                            objects.phealth -= int(self.damage)
                            damageNumber(objects.x, objects.y - objects.size - 10, self.damage)
                            self.didDamage = True
            if self.didDamage:
                self.canDamage = False

        else:
            if self in GAMEOBJECTS:
                GAMEOBJECTS.remove(self)

    def draw(self):
        pygame.draw.circle(DISPLAYSURF, self.color, (int(self.x), int(self.y)), int(self.size), 4 * (4 < self.size))

    def __init__(self, x, y, damage, blastRad=60, color=BLUE, type=[], duration=5, tickrate=0.6):  # add color specification some day =)
        self.x = x
        self.y = y
        self.size = 0
        self.pspeed = 100
        #self.type = ['playerSpell']
        self.type = type
        self.damage = damage
        self.casted = False
        self.starttime = time.clock()
        self.blastRad = blastRad
        self.color = color
        self.duration=duration
        self.tickrate=tickrate
        self.canDamage = True
        print(color)
        GAMEOBJECTS.append(self)

        # print('itsabomb!')

###################################################### DirtBG enemy classes:
# Bombsetter/explosion seem functional, two abilities to go!
#
#
#
class dirtEnemy(object):#Creating basic enemy

    def iterate(self):
        if time.clock()-self.startTime > 5 and not self.doingStuff:
            self.doingStuff = True
            self.startTime += 5
        if self.phealth <1:
            #GAMEOBJECTS.remove(self)
            startGame()
        ###--------------------------------------------------------------------------------------------------------------------
        #  Collision signalling and detection, this code should be in all objects that can collide
        #  Consequently, all colliding objects need size and position variables
        #  Checks whether the type list of other objects initialized by the program contains "collides"
        #  If yes, runs a collision check
        for objects in GAMEOBJECTS:
            if 'collides' in objects.type and 'enemy' not in objects.type:
                if 'projectile' in objects.type:
                    pass
                elif ((self.x - objects.x))*((self.x - objects.x)) + ((self.y - objects.y))*((self.y - objects.y)) < ((self.size + objects.size))*((self.size + objects.size)):
                    if 'player' in objects.type:
                        objects.phealth = self.dealDamage(objects.phealth)
        if self.doingStuff:
            self.useAbilities()


    def draw(self):
        if self.phealth != self.maxhealth:
            self.drawHP()
        pygame.draw.circle(DISPLAYSURF, GREEN, (int(self.x), int(self.y)), self.size)  # draw enemy

    def dealDamage(self, targethp):
        return(targethp-1)

    def drawHP(self):
        pygame.draw.rect(DISPLAYSURF, GREY, (self.x-self.size, self.y - self.size - 8, 2*self.size, 4))
        pygame.draw.rect(DISPLAYSURF, RED, (self.x-self.size, self.y - self.size - 8, 2*self.size*self.phealth/self.maxhealth, 4))

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def useAbilities(self):
        timeSinceSpawn = time.clock()-self.startTime
        if (timeSinceSpawn %10 > 1 and self.masterID==clientID and not self.idSet): #switch master client, master client decides current boss state on all clients (should be similar
            self.masterID = random.choice(self.masterIDs)
            broadcast('setDirtBossState(' + str(self.phealth) + ',' + str(self.masterID)+')')
            self.idSet=True
        if (timeSinceSpawn %10 < 1):
            self.idSet=False

        dropSwirlies = False
        setBombs = False
        castPool = False
        if 7 > timeSinceSpawn % 21 > 0 and self.canDropSwirlies:
            self.canSetBombs = True
            dropSwirlies=True
        if 14 > timeSinceSpawn % 21 > 7 and self.canSetBombs:
            self.canCastPool = True
            setBombs=True
        if 21 > timeSinceSpawn % 21 > 14 and self.canCastPool:
            self.canDropSwirlies = True
            castPool =  True

        for objects in GAMEOBJECTS:
            if 'player' in objects.type:
                [x, y] = translateCoordinates(self.x, self.y)
                [ox, oy] = translateCoordinates(objects.x, objects.y)
                if clientID == objects.ownerID: # Abilities that target all players
                    if setBombs:
                        self.canSetBombs = False
                        broadcast('dirtBossBombSetter(' + str(clientID) + ')')
                    if dropSwirlies:
                        RANGE = 800 #1500 px is size of dirtboss map, should be twice the numerical value in tarx/tary
                        self.canDropSwirlies = False
                        tarx = random.random()*RANGE - RANGE/2 + self.x  # Swirlos centered on boss
                        tary = random.random()*RANGE - RANGE/2 + self.y
                        [tx, ty] = translateCoordinates(tarx, tary)
                        broadcast('dirtBossSwirlies(' + tx + ',' + ty + ')')
                    #broadcast('basicEnemyZombie(' + x + ', ' + y  +')')
                    #broadcast('basicEnemyProjectile(' + x + ', ' + y + ', ' + ox + ', ' + oy +')')
                if self.masterID == clientID: # Abilities that target only one player
                    if castPool:
                        self.canCastPool = False
                        broadcast('dirtBossPoolDrop(' + str(clientID) + ')')

    def __init__(self, x, y, masterID):
        #print('HELLO HELLO HELLO')
        self.x = x
        self.y = y
        self.idSet=False
        self.pvleft = 0
        self.pvright= 0
        self.pvup= 0
        self.pvdown = 0
        self.pvx =0
        self.pvy=0
        self.canCastPool = True
        self.canSetBombs = True
        self.canDropSwirlies = True
        self.doingStuff = False
        players = []
        self.masterIDs = []
        for objects in GAMEOBJECTS:
            if 'player' in objects.type:
                players.append(objects)
                self.masterIDs.append(objects.ownerID)
        self.maxhealth = 3000 * len(players)
        self.phealth = 3000 * len(players)
        self.size = 50
        self.pspeed = 45  # speed in pixels/second
        self.prevID = '0'
        self.type = ['dirtBoss', 'collides', 'pdamaging', 'enemy']
        self.canShoot = True
        self.startTime = time.clock()
        self.masterID=masterID
        GAMEOBJECTS.append(self)
        # start timer

class dirtBossPoolDrop(object):
    def draw(self):
        # print('seconds elapsed =', time.clock()-self.starttime)
        factor = (time.clock() - self.starttime) / self.duration
        pygame.draw.arc(DISPLAYSURF, WHITE,
                        (int(self.x) - self.size, int(self.y) - self.size, 2 * self.size, 2 * self.size), 0,
                        (2) * math.pi, 4 * (4 < self.size))
        pygame.draw.arc(DISPLAYSURF, self.color,
                        (int(self.x) - self.size, int(self.y) - self.size, 2 * self.size, 2 * self.size), 0,
                        (2) * math.pi * factor, 4 * (4 < self.size))
        for i in range(0,25):
            tarx = int(self.x + random.random()*30)-15
            tary = int(self.y + random.random()*30)-15
            pygame.draw.line(DISPLAYSURF, self.color, (tarx, tary), (tarx+1, tary), 1)

        # pygame.draw.arc(screen, (255, 255, 255), (50, 50, 50, 50), 10, 20, 1)

    def iterate(self):
        self.x = self.target.x
        self.y = self.target.y
        if time.clock() - self.starttime > self.duration:
            dirtBossPool(self.x, self.y)
            GAMEOBJECTS.remove(self)

    def __init__(self, targetID):
        # print('HELLO HELLO HELLO')
        self.tstart = time.clock()
        self.pspeed = 70 / FPS  # speed in pixels/second
        self.normspeed = self.pspeed
        self.target = clientID
        self.color = (50,0,0)
        self.duration = 5
        self.starttime = time.clock()
        for objects in GAMEOBJECTS:
            if 'player' in objects.type:
                if objects.ownerID == targetID:
                    self.target = objects
        self.x = self.target.x
        self.y = self.target.y
        self.size = 50
        self.prevID = '0'
        self.type = []
        GAMEOBJECTS.append(self)

class dirtBossPool(object):
    def iterate(self):
        lifetime = time.clock() - self.starttime
        if 2 < lifetime < 1200:
            self.size =int(120 + self.blastRad * (lifetime-2) / 30) #grows 100 px/min in radius

            if time.clock() - self.dotTime > 0.5: #Damage 2x/sec
                self.dotTime += 0.5
                for objects in GAMEOBJECTS:
                    if 'player' in objects.type:
                        if ((self.x - objects.x)) ** 2 + ((self.y - objects.y)) ** 2 < (
                        (self.size)) ** 2:
                            objects.phealth -= int(self.damage)
        else:
            self.size = int(120 * (lifetime/2))
            for objects in GAMEOBJECTS:
                if 'player' in objects.type:
                    if time.clock() - self.dotTime > 0.5:  # Damage every 1sec
                        self.dotTime += 0.5
                        if ((self.x - objects.x)) ** 2 + ((self.y - objects.y)) ** 2 < (
                                (self.size)) ** 2:  # what the fuck
                            objects.phealth -= int(self.damage)

    def draw(self):
        pygame.draw.circle(DISPLAYSURF, self.color, (int(self.x), int(self.y)), self.size)

    def __init__(self, x, y, blastRad=100, color=(50,0,0), type=[], damage=35,
                 startSize=0):  # add color specification some day =)
        self.x = x
        self.y = y
        self.startSize = startSize
        self.size = 0
        self.pspeed = 100
        # self.type = ['playerSpell']
        self.type = type
        self.damage = 5
        self.casted = False
        self.starttime = time.clock()
        self.dotTime = self.starttime
        self.blastRad = blastRad
        self.color = color
        # print('itsabomb!')
        GAMEOBJECTS.append(self)

class dirtBossBombSetter(object):
    def draw(self):
        #print('seconds elapsed =', time.clock()-self.starttime)
        factor = (time.clock()-self.starttime)/self.duration
        pygame.draw.arc(DISPLAYSURF, WHITE, (int(self.x)-self.size, int(self.y)-self.size, 2*self.size, 2*self.size), 0, ( 2)*math.pi ,4*(4<self.size))
        pygame.draw.arc(DISPLAYSURF, self.color, (int(self.x)-self.size, int(self.y)-self.size, 2*self.size, 2*self.size), 0, ( 2)*math.pi*factor, 4*(4<self.size))

        #pygame.draw.arc(screen, (255, 255, 255), (50, 50, 50, 50), 10, 20, 1)
    def iterate(self):
        self.x = self.target.x
        self.y = self.target.y
        if time.clock() - self.starttime > self.duration:
            dirtBossExplosion(self.x, self.y)
            GAMEOBJECTS.remove(self)
    def __init__(self, targetID):
        # print('HELLO HELLO HELLO')
        self.tstart = time.clock()
        self.pspeed = 70 / FPS  # speed in pixels/second
        self.normspeed = self.pspeed
        self.target = clientID
        self.color = RED
        self.duration = 5
        self.starttime = time.clock()
        for objects in GAMEOBJECTS:
            if 'player' in objects.type:
                if objects.ownerID == targetID:
                    self.target = objects
        self.x = self.target.x
        self.y = self.target.y
        self.size = 50
        self.prevID = '0'
        self.type = []
        GAMEOBJECTS.append(self)

class dirtBossExplosion(object):

    def iterate(self):
        lifetime = time.clock() - self.starttime
        if lifetime < 2.5:
            self.size = self.startSize + int(self.blastRad*(lifetime))
            for objects in GAMEOBJECTS:
                if 'player' in objects.type:
                    if ((self.x - objects.x))**2 + ((self.y - objects.y))**2 < ((self.size + objects.size))**2 and ((self.x - objects.x)) **2 + ((self.y - objects.y)) **2 > ((self.size-15))**2: # what the fuck
                        if self.starttime not in objects.type:
                            objects.phealth -= int(self.damage)
                            objects.type.append(self.starttime)
        else:
            GAMEOBJECTS.remove(self)

    def draw(self):
        pygame.draw.circle(DISPLAYSURF, self.color, (int(self.x), int(self.y)), self.size, 4*(4<self.size))

    def __init__(self, x, y, blastRad = 1000, color=RED, type = [], damage=35, startSize=50): #add color specification some day =)
        self.x = x
        self.y = y
        self.startSize = startSize
        self.size = 0
        self.pspeed = 100
        #self.type = ['playerSpell']
        self.type = type
        self.damage = 35
        self.casted = False
        self.starttime = time.clock()
        self.blastRad = blastRad
        self.color=color
        print(color)
        #print('itsabomb!')
        GAMEOBJECTS.append(self)

class dirtBossSwirlies(object): #Draw needs to be written
    def draw(self):
        pi = math.pi
        angle = pi*(time.clock()-self.starttime)/2
        x1, y1 = rotate((0, 0), (1*self.size,0), angle)
        x2, y2 = rotate((0, 0), (1*self.size,0), angle + pi * 2 / 5)
        x3, y3 = rotate((0, 0), (1*self.size,0), angle + pi * 4 / 5)
        x4, y4 = rotate((0, 0), (1*self.size,0), angle + pi * 6 / 5)
        x5, y5 = rotate((0, 0), (1*self.size,0), angle + pi * 8 / 5)
        pygame.draw.line(DISPLAYSURF, (255,100,100), (self.x + x1*0.2, self.y + y1*0.2), (self.x + x1, self.y + y1), 4)
        pygame.draw.line(DISPLAYSURF, (255,100,100), (self.x + x2*0.2, self.y + y2*0.2), (self.x + x2, self.y + y2), 4)
        pygame.draw.line(DISPLAYSURF, (255,100,100), (self.x + x3*0.2, self.y + y3*0.2), (self.x + x3, self.y + y3), 4)
        pygame.draw.line(DISPLAYSURF, (255,100,100), (self.x + x4*0.2, self.y + y4*0.2), (self.x + x4, self.y + y4), 4)
        pygame.draw.line(DISPLAYSURF, (255,100,100), (self.x + x5*0.2, self.y + y5*0.2), (self.x + x5, self.y + y5), 4)

    def iterate(self): #calculate which player is closest. Then, if the closest player is this client, perform usual checks
        if time.clock()-self.starttime > 5:
            soaked = False
            closestplayer = 0
            tmpDistance = 999999999
            for objects in GAMEOBJECTS:
                if 'player' in objects.type:
                    calc = (objects.x-self.x)**2 + (objects.y - self.y)**2 #distance from swirly to player
                    if  calc < tmpDistance:
                        tmpDistance = calc
                        closestplayer = objects
                    else:
                        pass
            if closestplayer.ownerID == clientID:
                if (closestplayer.x-self.x)**2 + (closestplayer.y - self.y)**2 < (self.size + closestplayer.size )**2:
                    soaked = True
                    GAMEOBJECTS.remove(self)
            else:
                soaked = True
                GAMEOBJECTS.remove(self)
            if not soaked: # change this to broadcast
                [tarx, tary] = translateCoordinates(self.x,self.y)
                #dirtBossExplosion(self.x, self.y, startSize=0)
                broadcast('dirtBossExplosion('+tarx+','+tary+',startSize=0)' )
                GAMEOBJECTS.remove(self)


    def __init__(self, x, y):
        # print('HELLO HELLO HELLO')
        self.tstart = time.clock()
        self.pspeed = 70 / FPS  # speed in pixels/second
        self.finished = False
        self.normspeed = self.pspeed
        self.x = x
        self.y = y
        self.starttime=time.clock()
        self.damage = 20
        self.size = 25
        self.exploded = False
        self.prevID = '0'
        self.type = []
        GAMEOBJECTS.append(self)

def setDirtBossState(hp, masterID):
    for objects in GAMEOBJECTS:
        if 'dirtBoss' in objects.type:
            objects.masterID = masterID
            objects.phealth = hp

###################################################### Map classes
# Workflow as such:
# Create mapTrigger square with appropriate text that outputs the map image file path
# Add argument to startGame and background corresponing to file name
# spawn all of the objects in startGame,
# spawn invisible walls in background where you want them. 
class mapTriggerSquare(object):

    def iterate(self):
        for objects in GAMEOBJECTS:
            if 'player' in objects.type:
                if abs(objects.x - self.x) < self.size and abs(objects.y - self.y) < self.size:
                    startGame(self.mapPath)

    def draw(self):
        pygame.draw.rect(DISPLAYSURF, GREY, (self.x - self.size, self.y - self.size, 2 * self.size, 2 * self.size))

    def maketext(self):
        text = ''
        if self.mapPath == 'forest.jpg':
            text += 'BasicEnemy \nIt\'s very basic \nPractice here'
            stringOnScreen(self.x - 70, self.y - 70, text, duration=3000)
        if self.mapPath == 'dirtBG.png':
            text += 'Freaky wizard  \nVery deadly! \n designed for \narbitrary number \nof players'
            stringOnScreen(self.x - 70, self.y - 70, text, duration=3000)

    def __init__(self, x, y, mapPath):
        self.x = x
        self.y = y
        self.mapPath = mapPath
        self.maxhp=100
        self.phealth = 100
        self.size = 80
        self.pspeed = 120  # speed in pixels/second
        self.prevID = '0'
        self.type = ['mapTrigger'] #objects that collide must have size and position, x, y, z variables
        GAMEOBJECTS.append(self)
        self.maketext()

class background(object):#creating the background

    def iterate(self): #draws background. This should be first in GLOBAL
        global bgx, bgy
        bgx = self.x
        bgy = self.y

    def draw(self):
        if self.mappath == 'null':
            DISPLAYSURF.fill(BLACK) #rewrite this somehow using image and .blit? (in hope of better speed) Surely that should not be faster
        else:
            DISPLAYSURF.fill(BLACK)
            DISPLAYSURF.blit(self.image, (self.x + self.OFFSETX, self.y + self.OFFSETY))

    def __init__(self, mappath = 'null'):
        self.prevID = '0'
        self.type = ['bg']
        self.x = 0 # always start these at 0, they are used to keep track of absolute coordinates relative to other clients.
        self.y = 0
        global bgx, bgy
        bgx = self.x
        bgy = self.y
        self.pspeed = 0
        self.size = 0
        self.mappath=mappath
        self.OFFSETX = 0 #
        self.OFFSETY = 0
        self.type=[]
        if mappath != 'null':
            self.image = pygame.image.load(mappath)
        if mappath == 'desert.jpg': #place maps like so
            self.image = pygame.transform.scale(self.image, (1500, 1500))
            self.OFFSETX = -750 + WINDOWWIDTH/2
            self.OFFSETY = -750 + WINDOWHEIGHT/2
            invisibleWall(self.OFFSETX,self.OFFSETY+750, 30, 1000) #Lefthand side
            invisibleWall(self.OFFSETX+1500,self.OFFSETY+750, 30, 1000)#righthand side
            invisibleWall(self.OFFSETX+750,self.OFFSETY, 1000, 30) #upper side
            invisibleWall(self.OFFSETX+750, self.OFFSETY + 1500, 1000, 30) #lower side
        if mappath == 'dirtBG.png': #place maps like so
            SCALE = 1000
            self.image = pygame.transform.scale(self.image, (SCALE, SCALE))
            self.OFFSETX = -SCALE/2 + WINDOWWIDTH/2
            self.OFFSETY = -SCALE/2 + WINDOWHEIGHT/2 - 100
            invisibleWall(self.OFFSETX,self.OFFSETY+SCALE/2, 30, 1000) #Lefthand side
            invisibleWall(self.OFFSETX+SCALE,self.OFFSETY+750, 30, 1000)#righthand side
            invisibleWall(self.OFFSETX+SCALE/2,self.OFFSETY, 1000, 30) #upper side
            invisibleWall(self.OFFSETX+SCALE/2, self.OFFSETY + SCALE, 1000, 30) #lower side

        GAMEOBJECTS.append(self)

def startGame(mapPath = ''): #Initiates game objects for different maps.
    #print(mapPath)
    if mapPath == 'dirtBG.png':
        GAMEOBJECTS[:] = []
        broadcast('GAMEOBJECTS[:] = []')
        broadcast('background(\'dirtBG.png\')')
        # Below is codeword for broadcast('broadcast(\'player(ownerID = \' + str(clientID) + \')\')')
        broadcast('hopufhsoefy4089hfhjnapöiwhrf083rj2389fh8903hr')  # tfw code unreadable
        sleep(1)
        broadcast('dirtEnemy(WINDOWWIDTH//2,WINDOWWIDTH//6,'+ str(clientID)+')')
    elif mapPath == 'forest.jpg':
        GAMEOBJECTS[:] = []
        broadcast('GAMEOBJECTS[:] = []')
        broadcast('background(\'desert.jpg\')')
        # Below is codeword for broadcast('broadcast(\'player(ownerID = \' + str(clientID) + \')\')')
        broadcast('hopufhsoefy4089hfhjnapöiwhrf083rj2389fh8903hr')  # tfw code unreadable
        sleep(1)
        broadcast('basicEnemy(WINDOWWIDTH//2,WINDOWWIDTH//6)')
    else:
        GAMEOBJECTS[:] = []
        broadcast('GAMEOBJECTS[:] = []')
        broadcast('background()')
        # Below is codeword for broadcast('broadcast(\'player(ownerID = \' + str(clientID) + \')\')')
        broadcast('hopufhsoefy4089hfhjnapöiwhrf083rj2389fh8903hr')  # tfw code unreadable
        broadcast('mapTriggerSquare(WINDOWWIDTH//2,WINDOWWIDTH//6,\'forest.jpg\')')
        broadcast('mapTriggerSquare(WINDOWWIDTH//3,WINDOWWIDTH//6,\'dirtBG.png\')')


if __name__ == '__main__':
    main()
