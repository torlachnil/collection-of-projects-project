import socket
import sys
import threading


class Client(threading.Thread):
    def __init__(self, ip, port, connection):
        threading.Thread.__init__(self)
        self.connection = connection
        self.ip = ip
        self.port = port

    def run(self):
        while True:
            data = self.connection.recv(8092*3)
            if data:
                s.send_to_all_clients(data)
            else:
                continue
        self.connection.close()


class Server:
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        self.address = (self.ip, self.port)
        self.server = None
        self.clients = []

    def send_to_all_clients(self, msg):
       # print('There are', len(self.clients), 'clients')
        for client in self.clients:
            if(b'setPlayerState' not in msg):
                print(msg)
            try:
                client.connection.send(msg)
            except ConnectionResetError:
                client.connection.close()
                self.clients.remove(client)

    def send_to_client(self, ip, port, msg):
        for client in self.clients:
            if client.ip == ip and client.port == port:
                client.connection.send(msg)

    def open_socket(self):
        try:
            self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.server.bind(self.address)
        except socket.error:
            if self.server:
                self.server.close()
            sys.exit(1)

    def run(self):
        self.open_socket()
        self.server.listen(5)

        while True:
            connection, (ip, port) = self.server.accept()

            c = Client(ip, port, connection)
            c.start()

            self.clients.append(c)

        self.server.close()

if __name__ == '__main__':
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    print(s.getsockname()[0])
    thisIP = s.getsockname()[0]
    s.close()
    s = Server(thisIP, 6666)
    s.run()
