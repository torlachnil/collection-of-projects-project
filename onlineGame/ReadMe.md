## OnlineGame: design

This is a computer game I wrote in the summer of 2017 as a way to learn Python. I recorded a demo of the game here:  https://youtu.be/xsx56zP0tNw

I suggest that one reads **How to play the game** before watching the video if one wants a clue what is going on. In this demo video I do not 
showcase multiplayer.

The game runs on the PyGame engine, and uses Socket for communication across the web. 
With correctly configured IP's the multiplayer works. 

The current version seems a bit unstable. Currently, the game is unable to open 
the background art for its maps. 

The main idea was to create a 2d game that has a gimmicky gameplay system
where one draws "runes" with their mouse to cast spells. Spells consist of a 
base spell such as "Fireball" or "Blizzard", and a sequence of up to four 
supporting runes that can increase damage, area of effect or the number of 
fired projectiles. The idea is the for players to cooperate in defeating 
bosses, whose mechanics are largely inspired by World of Warcraft raids, 
but with the healing and tank roles taken away. 

Currently two bosses exist. One has three mechanics rotating on a 
relatively tight timer, while the other one is a very simple boss
meant for testing that player mechanics (mainly, spells) function correctly.

To minimize input latency, the player client simulates all player actions 
locally before sending them to the server (which then updates all other clients). 
One of the connected clients is designated the master client, and all computer
controlled entities on all other clients have their state periodically synchronized
to this master client. The role of master client is periodically swapped. 
The goals of this design were to:
    A: Minimize experienced input lag (which is exteremely important for the
    gameplay experience)
    B: Ensure that clients are in sync during multiplayer. 
The downfall is that if latency is high (>50-100ms) players will see computer 
controlled characters status acting in a way incosistent with what is happening
on the local client for fast moving targets. This should not be a problem in 
PvE (Player versus Environment) content, since such enemies often move slowly.
If I were to extend this game in the future, this means that the design space
of fast enemies is a dangerous one, that likely will simply demand low latency
or a significantly more complicated netcode solution.

## How to run the game
DISCLAIMER: The server and game combination run what I think to be safe code.
            The server echoes any string it is sent, and the client checks 
            recieved strings against a dictionary from which it then executes
            code. 
First, you run altServer.py . It will print out its global IP (as seen by any
computer that connects to the internet.) Then, go into blankgame.py and edit
line 45 ("serverIP = 'xxx.xxx.xxx.xxx'") to use the IP of altServer. Now run 
one or several copies of the client on one or several computer and enjoy the 
online experience. A future feature will take the IP as an argument when running
the game, probably via the usual prompt -> text parsing i/o.

## How to play the game

Movement is with WASD keys

Spellcasting works as follows: 
you press SHIFT, and a 3x3 grid pops up. While you hold shift, you can supply up to five 
"runes" in succession. The first rune is a "primary" rune, and determines what spell 
you cast. The following runes must be "support runes", which augment your spell in 
various ways (adding damage and additional effects). 

The current set of runes are by the following lines of code. 

    startends[0][6] = 'pFireball' <------ This is a Primary spell
    startends[6][8] = 'pPowerUp'
    startends[1][1] = 'blastUp'
    startends[2][8] = 'multicast'
    startends[7][5] = 'dash' <------ This is a Primary spell
    startends[0][8] = 'blizzard' <------ This is a Primary spell
    startends[2][6] = 'CWDT' <------ This is short for "cast when damage taken"

If you have played 'Path of Exile', you will notice the influence it had on the spell system design. 

startends is a 9x9 matrix of possible start-and-end tiles in the 3x3 grid. The grid
is labelled as follows:


|:---:|:---:|:---:|
| 0 | 1 | 2 |
| 3 | 4 | 5 |
| 6 | 7 | 8 |

To draw the rune **'pFireball'** the sequence of operations is to:
- Hold down SHIFT
- Move your mouse to the **0th** box of the grid (upper left)
- Press and hold the left mouse button (LMB)
- While holding down LMB, move your mouse to the box labelled **6**
- Release SHIFT
- Next time you left click, your spell will deploy in the direction of your cursor

The intention is that in the future, spelldamage will scale based on how well you approximate 
some predetermined path, instead of just giving you the full damage spell if you start and end 
your rune in the correct box. 

