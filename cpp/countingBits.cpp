#include <iostream>
#include <vector>
using namespace std;

/* 
Given a number n, given on prompt, prints a list containing the number of
1's in the binary representations of [0, 1 ... n]. 

What did solving this problem teach me? bad_alloc seems to mean "index out of bounds"
(among other things, going by stackexchange)
*/

class Solution {
public:
    /* A fast solution uses that every time a new factor of 2 is reached
    Previous pattern with +1 is repeated. This sounds like recursion to me
    */
    vector<int> buildReturn(vector<int> bitCounts, int num) {
        int size = bitCounts.size();
        bool exit = (2 * size > num + 1);
        // Recursive step - repeat what has happened until now with 1 extra bit as 1
        // + 1 since we want to end on num but we index by 0. In the exit case we
        // only add the elements that we still need. 
        int extSize = (exit ? (num-size + 1) : size);
        vector<int> extension(extSize);
        for (int i = 0; i < extSize; i++)
        {
            extension[i] = bitCounts[i]+1;
        }
        bitCounts.insert(bitCounts.end(), extension.begin(), extension.end());
        return exit ? bitCounts : buildReturn(bitCounts, num);
    }

    vector<int> countBits(int num) {
        if (num == 0){
            vector<int> ret{0};
            return ret;
        }
        vector<int> retVector{0, 1};//Start recursion using base case
        retVector.reserve(num+1);
        return buildReturn(retVector, num);
    }
};

int main() {
    cout << "Type a number, get bitCounts. I expect to crash unless you give me an int.\n > ";
    int arg;
    cin >> arg;
    cout << endl;

    Solution solution;
    vector<int> printArray = solution.countBits(arg);
    for (int i = 0; i <= arg; i++)
    {
        cout << printArray[i];
    }
    cout << endl;
    return 0;
}