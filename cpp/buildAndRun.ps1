#See README.md for environmental prerequisites to running this script.
echo "Assuming that this script was called from the cpp folder!"
cd build
cmake ..\
#Cmake gets the automatic platform configuration wrong on the first run with an
#Empty build folder, so it finds no libraries and can not write build information.
cmake ..\
Invoke-MsBuild -Path .\selfLearnyGame.vcxproj
cd ..\bin\Debug\ #Go to and run executable
./selfLearnyGame.exe
cd ..\..\ #Return to cpp "root" folder