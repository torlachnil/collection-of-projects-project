# C++ learning folder!

**Base observation:** A lot of ML libraries are implemented in C++. In addition, I know from experience that the pygame engine is pretty slow, so a self learning actor playing a silly game of my own design seems more doeable in c++. Thus, C++ should be learned. 

## Design intent and purpose of selfLearnyGame

The main idea is to write a computer game that can be played by various ML models/algorithms. First, a simple game will need to be written. The game can not reward extremely high APM since this strongly favors the computer, so I am opting for controlling a single small player object to avoid approaching dangerours objects. Depedning on how stiff I make the controls (how fast can the player turn?) I can demand more planning from the AI. 

* The game needs to have a good API towards the ML algorithm - providing a training value (survival time in frames?), the current game state and a way for the AI to provide input (keyboard commands)
* There are two options regarding how to sync the AI and the game:
    * Let the AI run concurrently and poll the game state at-will
        * **Pro** Uses more cores, my computers have cores. 
        * AI free to poll as often as it wants, could even fit an "optimal polling rate". 
        * **Con** If I want to speed up the game relative timings can get messy, even if I try to keep some common "time unit" between the game and AI. Order of operations and "real time deltas" in the game world in relation to polling time deltas will be out of whack and the AI might not generalize to the game simulated at normal human playable speed. 
    * Sync the AI to the game clock - giving it the stability of a frame-by-frame timeseries as in-data. 
        * **Pro** Not concurrent, network could just be called from game code and be asked for a next input. Order of operations would be ensured, and one network output will always represent the iteration from one frame to the next. If doing a genetic algorithm I could still parallelize training by having Ncores instances of training running in parallel a few times before comparing the results and deciding what models to keep. 
        * Is there a point to this? For every frame in the series we add a lot of of extra work to ex. a neural network. Couldn't time since last polling be one of the inputs to the AI? Could even be provided by the API. 

## Running anything in here
I suppose .exe's will work on any machine if you trust that I put an executable that has anything to do with my code in the gitlab folder. Otherwise, have fun replicating my environment, future me!

## Getting C++ to link libraries on windows was a terrible experience
The main idea was to use **vcpkg integrate install** plus the line
    set(CMAKE_TOOLCHAIN_FILE "C:\\Users\\torla\\vcpkg\\scripts\\buildsystems\\vcpkg.cmake")
in the CMake config file to combine cmake with the vcpkg package manager. The end result ... uh .. works, but it is making me feel like I should have just installed \<insert linux distro here\>. The important part is that now I can execute my toolchain (CMake to generate build config, Invoke-MsBuild to compile & link) from the command line, and I could even put it in a script (buildAndRun.ps1). 

In principle, all I need to make this run on a Unix system is to commit everything except the Build folder to git, clone from a unix machine, cmake autodetects make+gcc as the target building solution, and I write a "buildAndRun.sh" script that executes the unix toolchain (probably Make).

(see https://github.com/microsoft/vcpkg/issues/9339)

Roughly speaking, to get this environment working in vsCode I had to:
* install vcpkg
* install cmake
* use vcpkg integrate install once per target platform
* explicitly install 64 bit versions of my desired libraries
* **At this point the Cmake portion of the pipeline seems to work - it seems to successfully find the SDL2 library installed by vcpkg** 
On Windows, to build I needed to:
* Download the MSVC build toolset to get msBuild
* Install Visual Studio 2019 because it comes bundled with necessary cmake infrastructure for c++ building on windows (One of the "workloads" one can pick when installing)
* Install "Invoke-MsBuild" via the powershell command
    Install-Module -Name Invoke-MsBuild -RequiredVersion 2.6.2
* Build by standing in the /Build folder and using 
    Invoke-MsBuild selfLearnyGame.vcxproj
In powershell. (Existence of this alias depends on using Import-Module ... first, I managed to put it in a configuration file somewhere in my oneDrive, which is apparently automatically sourced by powershell.)
* **Finally**, SDL shadows the main method somehow, so to get everything working (allow the linker to find my main() method), stackexchange tells me I need to #undef main.

Once all of these (totally simple) prerequisites have been satisfied, the buildAndRun.ps1 powershell script will build and run the project for you (although I am not one hundred percent sure how often you *really* need to rerun cmake)

* **TODO:** Get the file strucutre under control

