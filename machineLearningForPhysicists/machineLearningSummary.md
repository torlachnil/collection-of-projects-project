
### Summary of key points in Machine Learning For Phycisists
Also, some thoughts/questions that popped up along the way. 

## Why prediction is hard / Basics of statistical learning theory

* Machine learning is apparently glorified regression.

* Complex models diverge fast. This means that extrapolation is a huge problem. (Example: when fitting polynomials $f_i(x_i)$ to training data for $x \in [0, 10]$), we know that different order polynomials will change by many order of magnitude at $x = 100$)

* We can consider two error measures - the in-sample $E_{in}$ and out-of-sample errors $E_{out}$. These two approach the same value for a large number of samples. For a given model there is a minimum achievable error, which is called the **bias** of the model. This is a measure of how far the model function is from representing the true function underlying the sample. For nonzero bias, the in-sample error approaches the bias from below as the number of data points increases (You start at zero error at $N=1$ samples since anything can fit a point). The out of sample error approaches the bias from above (if you have only one point, you will be arbitrarily bad at guessing how the function changes so the out of sample error will be arbitrarily large)

* There is a so-called bias-variance tradeoff. Let us approximate model complexity as the number of free parameters in the model. Given this, a more complex model always has a lower in-sample error $E_{in}$ (You can probably imagine why having more parameters is a strict improvement). For an infinite sample size, an arbitrarily complex model (that lives in the same function space as the true function that we are sampling) will have $E_{out} = E_{in} = 0$, i.e. zero bias. Given a finite set of samples, we know that more complex functions tend to diverge faster outside the training data, so $E_{out}$ increases with complexity. The bias-variance tradeoff basically tells us that if we want to reduce **bias** (increase model complexity to get closer to the true function), we will have increased **variance** (which here refers to $E_{out}$ minus bias). 

* The previous point means that to minimize the out-of-sample error $E_{out}$ with a finite number of sample points, we want to increase model complexity only until the increase in variance starts outweighing the decrease in bias. 

* **Thought:** There is some foreshadowing of "regularization". Makes me wonder if ML has an equivalent of "renormalization" where we fit a very complex model to get down bias and then somehow "coarse grain" it to subdue variance while keeping some properties that a fit with the coarse grained model wouldn't have had if applied directly. 

* The out of sample error can be written as $E_{out} = Bias^2 + Variance + Noise$ (the so-called bias-variance decomposition). 

## Gradient Descent

* Minimizing cost function (E_in, E_out) given some model $M(X, \theta_i)$ where $X$ is the sample and $\theta_i$ is the set of model parameters is a multivariate optimization problem in $\theta_i$. Gradient descent uses that $\nabla_\mu \phi(\theta_\mu)$ is a vector pointing in the direction of fastest growth of the scalar field $\phi$. By iterating according to $\theta_{k+1} = \theta_\mu^k - \eta \nabla_\mu \phi(\theta_\mu^k)$ we are iterating in the direction of steepest descent. **It is very useful to think of this as a particle rolling on a potential.**

* The parameter $\eta$ is the step size (or in ML, *learning rate*). If it is very small, convergence is slow. If it is large, the algorithm is unstable. In general, we want a dynamic learning rate, and there is an optimal method based on taylor expanding (\theta_k^\mu + \eta^\mu) to second order and choosing $\eta^\mu$ as the solution to $2nd order polynomial(\eta) = 0$. This is **Newton's method**. Note that we now pick one $\eta$ for each coordinate, and that we need to compute not N but N^2 derivatives (the tensor $\partial_\mu \partial_\nu$) where $N$ is the number of parameters. VERY EXPENSIVE!

* The natural scales that determine good $\eta$ is the magnitude of the gradients. To ensure stability you want $\eta < \frac{2}{\lambda}$ where $\lambda$ is the largest eigenvalue of the Hessian matrix (\partial_\mu \partial_\nu \phi). 

* Another pitfall is so-called *false minima*. Gradient descent will just push your initial condition down into some local minimum, since the method itself is local (derivatives). The way to get around this is to introduce some random noise in the iteration process, so that we can jiggle our way out of (shallow) local minima in parameter space. 

* There are some named approximations to the optimal method. They usually involve "momentum", which in the "particle rolling on a potential" picture can be mapped to giving the particle mass, as well as some kind of randomness. The advantage of momentum is that long-lasting shallow descents can be traversed faster since the particle can accelerate in this direction without the gradient growing. 

* A way to reduce computational cost (especially for the optimal method) is so-called "mini-batching". You pick only a few parameters at a time (a "batch") and you iterate these using Newton's. Minibatching can also be done with a non-quadratic algorithm, but there are no savings unless the scaling is faster than N. That being said, minibatching enables paralellism which is good. Then you do this for all batches. By randomizing the batches, you get something that behaves a lot like the optimal algorithm with some natural statistical noise. This method is bad at long shallow ravines and saddle points. It is known in the mathemathics literature that saddle points are very common for scalar functions in high dimensional spaces. 

* **Thought:** The desire to reduce oscillations and increase convergence rates is reminiscent of control theory. Is the issue here that most "good" control theory algotrithms scale badly with the number of inputs? The first order have the following issues (P is nonlocal, I is literally just momentum, D is roughly the Nesterov Accelerated Gradient) i.e. they do not give us anything new. Is Kalman too expensive? Do we really know the "dynamics" of the model on the learning potential? I doubt the cost is better than N^2. 

* **In practice** Someone will have written nice optimization packages, probably you can read up on some general properties and pick something good for your dataset.

## Overview of Bayesian Inference

* Bayesian inference has three main ingredients: the likelihood function $p(X|\theta)$ which describes the probability of observing the dataset $X$ given the parameters $\theta$, the prior distribution $p(\theta)$ which describes what we know about $\theta$ before collecting any data and the posterior distribution 
$$
    p(\theta|X) = \frac{p(X|\theta)p({\theta)})}{\int d \theta' p(X|\theta')p({\theta')})}
$$
That describes the most likely parameters $\theta$ given the measured data. 

* Commonly, we pick the set of parameters that maximize the likelihood of the observed data, i.e. $\text{max}_\theta (p(X|\theta))$ as the optimal set. A less computationally expensive option (that does however not minimize the RMS error) is the *maximum-a-posteriori* (MAP) estimate  $\text{max}_\theta (p(\theta|X))$.

* Popular prior distributions $p(\theta)$ are the **gaussian** $p ~ e^{-k\theta^2}$ and **Laplace** $p ~ e^{-k |\theta|}$ distributions. These reflect the information that most parameters be small or zero respectively. 

* The maxima we are trying to find here are found using the optimization methods of the previous chapter. I.e. we are choosing the Bayesian likelihood as our cost function.

## Linear Regression

* Analytically solvable, can give us some interesting insights!

* Let $n$ be the number of points in sample, $\sigma$ the standard deviation of gaussian noise and $p$ the dimension of the data. Then the generalization error is
$$
    |E_{in} - E_{out}| = 2 \sigma^2 \frac{p}{n}\ .
$$
Notably, generalization is notably bad for high dimensional data (p large), so the algorithm doesn't learn! A second problem is that once $n>p$ we have problems with overdetermination of the least square method. 

* One idea is to use regularizers, which basically just means solving other problems than minimizing the RMS error. 

* It is really nice if you manage to make your optimization problem *convex*, since this means that any local minimum is also global. 

* It can be shown that maximum likelihood maximization in Bayesian inference is equivalent to minimizing RMS in linear regression. 

* Choosing a gausiann prior distribution and making a MAP estimate is equivalent to Ridge regression, a form of regularized linear regression. A similar result can be showed for LASSO (another regularization). 

* To recast ML problems into regression we define the **design matrix** $X$. Given $n$ measurements of data and $p$ input features per measurement, the design matrix is the $n \times p$ matrix containing all sets of measurements. Let $y_i$ denote the output data corresponding to the i:th measurement, here a scalar. Then, we want to find the weights vector $w$ that minimizes 
$$
    ||Xw - y||_2^2 = \text{min}_w (Xw - y)^T (Xw-y)\ .
$$
Note that the matrix problem most of the time is undetermined ($n < p$). Most of the time this means that $\mathbf{X}_{:,1},\cdots \mathbf{X}_{:,p}$ are most often linearly independent, and there exists unique solution to this problem:
$$
\hat{\textbf{w}}= (\mathbf{X}^T\mathbf{X})^{-1}\mathbf{X}^T \textbf{y}\ .
$$
This amounts to an orthogonal projection of $y$ onto the column space of X. The statement is then that $y_i$ = $w$ \cdot $X_{i,:}$, which gives us our (multi)linear regression. 

## Logistic Regression

* Logistic regression is used for discrete problems - such as classifying whether an image is a cat or a dog, or a measurement corresponds to a signal or noise. Specifically, one considers the response function
$$
    P(y_i=1|s) = \sigma(s) = \frac{1}{1 + e^{-s}} (= 1-\sigma(-s))\ ,
$$
the sigmoid. The sigmoid is nearly zero for large negative $s$ and roughly 1 for positive $s$, so kind of like a step function. The probability that something is classified as true is $\sigma$ and the chance that it is classifies as false is $1-\sigma$. $s$ here represents our product $X_i^T \cdot w$ from the regression chapter. 

* We can define the log-likelihood
$$
    C(w) = \sum_i -y_i \log \sigma(X_i^T\cdot w ) - (1-y_i) \log[1-\sigma(X_i^T \cdot w)]
$$
which is measure related to relative entropy of how far we are from estimating the correct distribution y(X_i). Minimizing this cost function reduces to
$$
    0 = \partial_w C(w) = \sum_{i=1}^n [\sigma(X_i^T w) - y_i] X_i
$$
Which has no analytical solution, but *is* convex so we do not have to be particularly smart about our gradient descent.

* There is a non-binary alternative to logistic regression called SoftMax. Basically, let $y_{i,m}$ have an extra index and let only one of them be nonzero. You then obtain something that looks almost like logistic regression, but with an extra index =). The SoftMax function is 
$$
    P(y_{i,m'} = 1|x_i, {w_k}_k^{M-1}) = \frac{e^{-x_i^T \cdot w_{m'}}}{\Sigma_m^{M-1} e^{-x^T_i \cdot w_m} }
$$
and the related cross entropy (that is to be minimized) is given by:
$$
    C(w) = -\sum_{i=1}^n \sum_{m=0}^{M-1} y_{i m} \log P(y_im = 1 | x_i, w_m) + (1-y_{im}) \log(1- P(y_im = 1 | x_i, w_m))
$$

## Ensemble methods

* It is popular to use "committee" type setups where several different models are trained on the same data, and a weighted combination of their predictions are used. 

* Statistical learning theory can be extended to ensembles, and the main result is that variance can in principle be brought to zero by using avery many uncorrelated linear learning methods on the same data. Specifically we arrive at the result
$$
Var(x) = \rho(x) \sigma^2_{\mathcal{L},\theta} + \frac{1-\rho(x)}{M} \sigma^2_{\mathcal{L},\theta}
$$
where $x$ is a new data point (from outside the training data), $\mathcal{L}$ is the training dataset, $\theta$ parametrizes the different models that we may train, $\sigma_{\mathcal{L}, \theta}$ is the variance with respect to choice of model of the predictor, $\rho(x)$ is the expectation (over models) of a cross-correlation between models parametrized by $\theta, \theta'$, and M is the number of different models in the ensemble. If we can add more models to the ensemble (increase $M$) while also keeping the model cross correlation $\rho(x)=0$, we can completely eliminate variance. **Since variance corresponds to overfitting, this means we have a concrete way of avoiding overfits**. 

* There are a few popular ensemble methods that should be presented. 
    * **Bagging:** Train the same model on differenct subsets of the data. Randomize (partially overlapping) subsets into a bunch of training sets. Fit the same model to all the datasets, and use the sum of all the trained models as your final model. This is particularly useful for models that are very unstable to small changes in the training data. 
    * **Boosting:** Bagging with different weights for each trained classifier. AdaBoost is an implementation of this concept. 
    * **Random Forests:** Random forests are essentially decision trees of linear classifiers. Decision trees are extremely high variance, a weakness that is specifically targeted by ensemble learning theory. The main reason why these are interesting is because there are several way of achieving good decorrelation between different trees. The three main methods are
        * Bagging (training the same tree of different subsets of data)
        * "Feature bagging", where you train the trees with access to only some of the inputs (features) at a time. You can do this at each split in the tree allowing for an even larger number of uncorrelated trees. 
        * "Extremized Random Forests" have more splits, and the feature bagging is done completely randomly (in contrast to using "optimality criteria") which is common in normal feature bagging. This reduces the predicitivy of each individual tree, but you have greater orthogonality so you can increase the number of members in the ensemble. 
    Tutorial at http://scikit-learn.org/stable/auto_examples/ensemble/plot_forest_iris.html
    * **Gradient Boosted Trees:** Start with a few decision trees and perform fits using bagging/feature bagging. Compute the gradient of the cost function with respect to the weight of each decision tree. Using a smart parametrization of decision trees, add decision trees that move us in the negative direction of the gradient. There is an implementation called XGBoost. 
    
## Introduction to neural networks

* We have seen that using several models an weighting their outputs is a good way to suppress variance (i.e. overfitting) of our models. Neural networks are a form of ensemble method where the individual models are called **neurons**. The second distinguishing feature is the presence of so-called **hidden layers**, which are model ensembles whose inputs are the outputs from other models. How to actually perform fitting in this situation is not obvious, the method for this is called **backpropagation**. 

* A linear combination of inputs is fed to each neuron, that then generates a single scalar output using some arbitrary function (for discrimination tasks a sigmoid or Heaviside function - for continuous esitmators a function that is zero below a threshold and otherwise linear is common). This output is sent forward to the next layer, and weighted for each target neuron. Exceptions to this basic structure exists (feedback loops). See this image:
![](neuralNetFromPaper.PNG)

* Neural networks (even of depth 1) are *universal* meaning they can approximate any output function given that they are allowed to have an arbitrary number nodes. This can be proven rigorously, but an intuitive proof is located at http://neuralnetworksanddeeplearning.com/chap4.html 

* Deep neural networks are absurdly popular since circa 2012, meaning there are a lot of high level libraries (Caffe, Keras, Pytorch, TensorFlow, etc.). This is due to a combination of universailty and real life applicability. 

# How to train a neural network: backpropagation

